package com.example.listapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ListView listView;


    String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry",
            "WebOS","Ubuntu","Windows7","Max OS X"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=findViewById(R.id.mobile_list);
      ArrayAdapter arrayAdapter=new ArrayAdapter<String>(this,R.layout.listviewexample,mobileArray);
       listView.setAdapter(arrayAdapter);
       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               String mobile=((TextView)view).getText().toString();

               Intent intent=new Intent(MainActivity.this,SingleIteamActivity.class);
               Toast.makeText(getApplicationContext(),"Selected"+position,Toast.LENGTH_SHORT).show();

                 intent.putExtra("mobile",mobile) ;
                 startActivity(intent);


           }
       });


    }
}
