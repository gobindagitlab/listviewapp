package com.example.listapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SingleIteamActivity extends AppCompatActivity {
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_iteam);
        textView=findViewById(R.id.TextView);

        Intent i=getIntent();
        String mobileset= i.getStringExtra("mobile");
         textView.setText(mobileset);

            }
}
